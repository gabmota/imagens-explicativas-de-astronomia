# Imagens explicativas de astronomia

Este repositório é uma coleção de imagens que ilustram conceitos de astronomia.

O objetivo desse repositório é agregar material de astronomia para que pessoas interessadas em astronomia (sejam elas estudantes, sejam amadores de astronomia, entre outras) possam ter acesso a imagens que providenciam um rápido modo de revisar conceitos astronômicos de forma ilustrativa e didática.

## Colaboração

Qualquer um é bem vindo para colaborar. Basta criar um pull request e explicar porque suas sugestões deveriam ser aceitas.

Para tanto, quando for enviar um Pull Request, as imagens devem:

- ter relação com astronomia
- não ter direitos autorais
- ser organizadas nas pastas adequadas
- ser nomeadas de acordo com o padrão Kebab (todas as letras minúsculas e palavras separadas por hífens)

## Contribuidores

Lista de contribuidores:

- André Souza Abreu (idealizador do projeto)

O projeto é oficialmente promovido pela [Liga Olímpica de Astronomia](https://ligaolimpicadeastronomia.com.br).

## Licença

O projeto está sobre a licença do MIT, o que significa que você pode usar, copiar, distribuir esse projeto do modo que quiser, desde que você mantenha a licença. Veja [licença][license] para maiores informações
